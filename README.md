Purpose:
    Controlling pre-configured docker-compose microservices via dialog menu.
    Usually, controlling of microservices from clear console some non comfortable. You need to configure and run/stop services using different commands or aliases.
    I think, we can make it more simle and comfortable by use dialog menus and makefiles.
    And more comfortable if put shared by services environment variables in one file.

Some most popular microservices configurations included:
    _Traefik_ - reverse proxy to control incoming connections. You can have very, very many web services with it.
    *MariaDb* database
    phpmyadmin
    Redis
    RabbitMQ
    php-fpm, include console access to install and manage symfony php framework
    nginx
    whoami - can be useful on configuring stage
    mailcatcher

Requerements:
    ssh access, docker, docker-compose, dialog, bash

Configuration:

    If you planned to use OAuth to login traefik dashboard, read
    https://support.google.com/webmasters/answer/9008080#html
    https://support.google.com/googleapi/answer/6158849?hl=en

    Confirm your domain on search.google.com by add TXT record to DNS or put given html token file from googleauth to ./googleauth/ to provide oauth login to traefik dashboard

    Configure app on console.cloud.google.com

    GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET - OAuth credentials from google cloud console

    OAUTH_SECRET: This is used to sign the cookie and should be random. Generate a random secret with:
    openssl rand -hex 16

    (https://mediadoma.com/google-oauth-tutorial-dlja-docker-i-traefik-autentifikacija-dlja-servisov)
    (https://www.smarthomebeginner.com/google-oauth-with-traefik-2-docker/)
    (https://developers.google.com/identity/protocols/oauth2)

And then:
    Configure nginx in etc/nginx/templates.all/ as you need (i do not have menu for it yet)
    Run make menuconfig or just make
    Set all options
    Check docker services to run (menu section 5)

USAGE:

    make build - some as - docker-compose n{-f $service} build
    make up - some as - docker-compose n{-f $service} up -d, rise up all checked services
    make down - some as - docker-compose n{-f $service} down, shutdown all checked services

    make user / make root - access to php-fpm container console
    make workers / make workers-root - symfony workers container console

http/https services:
    Traefik dashboard: https://domain/dashboard/ (slash on the end of url is mandatory, see traefik documentation)
    Whoami: http://whoami.domain
    Nginx: ~/www/html/public as http://domain
    phpmyadmin: host:8183
    mariadb: host:3306
    pma: host:8183
    rabbitmq: host:15672
    redis: host:6379
    mailcatcher: host:1025

Note:
    This repo contents sone files for use on x86 arch. I use it for "prod" (little telegram bot) and "test" purposes on home Intel Atom n270 machine under Debian.

use
    git clone --recurse-submodules git@bitbucket.org:jewrgen/evergiven.git
    To use on x86 arch
