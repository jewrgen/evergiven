SHELL := /bin/bash

menuconfig:
	./sbin/Menu MenuConfig

up:
	./sbin/Menu servicesUp

down:
	./sbin/Menu servicesDown

build:
	./sbin/Menu servicesBuild

php7:
	. .env; docker exec -i -t --user $${APP_USER} $${PROJECT_NAME}-php7-fpm bash

php7root:
	. .env; docker exec -i -t --privileged $${PROJECT_NAME}-php7-fpm bash

php81:
	. .env; docker exec -i -t --user $${APP_USER} $${PROJECT_NAME}-php81-fpm bash

php81root:
	. .env; docker exec -i -t --privileged $${PROJECT_NAME}-php81-fpm bash

nginx-root:
	. .env; docker exec -i -t --privileged $${PROJECT_NAME}-nginx bash

#workers:
#	. .env; docker exec -i -t --user $${APP_USER} $${PROJECT_NAME}-worker bash

#workers-root:
#	. .env; docker exec -i -t --privileged $${PROJECT_NAME}-worker bash
