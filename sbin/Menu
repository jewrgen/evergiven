#!/bin/bash
ENVFILE=./.env
SBINDIR=./sbin/
YAMLDIR=./dockerfiles/docker-compose
SERVICES_VAR_NAME="#SERVICES_LIST"

#Default xdebug.ini path
XDEBUG_INI=../../etc/ini/xdebug.ini

APP_USER=$(whoami)
APP_USERID=$(id -u)
APP_USERGID=$(id -g)
GOOGLEFILE=$(ls ./googleauth)

 #Define parameters for menu
 TERMINAL=$(tty) #Gather current terminal session for appropriate redirection
 HEIGHT=0
 WIDTH=0
 CHOICE_HEIGHT=0
 BACKTITLE="project configurator"
let SCREEN_WIDTH=170

#--------------------------------------------------------------------------------------------------------
#declare -a MenuEntries

declare -A MainMenu;                     declare -a menuorders;
MainMenu['0']='Environment (dev/test/prod)'; menuorders+=(0)
MainMenu['1']='Directory settings';      menuorders+=(1)
MainMenu['2']='Domain settings';         menuorders+=(2)
MainMenu['3']='Database settings';       menuorders+=(3)
MainMenu['4']='Google auth settings';    menuorders+=(4)
MainMenu['5']='docker-compose services'; menuorders+=(5)

declare -A DbMenu;                                    declare -a DbOrders;
DbMenu['DATABASE_PORT']='Db server port'            ; DbOrders+=('DATABASE_PORT')
DbMenu['DATABASE_NAME']='Db name'                   ; DbOrders+=('DATABASE_NAME')
DbMenu['DATABASE_USER']='Db user'                   ; DbOrders+=('DATABASE_USER')
DbMenu['DATABASE_PASSWORD']='Db user password'      ; DbOrders+=('DATABASE_PASSWORD')
DbMenu['DATABASE_ROOT_PASSWORD']='Db root password' ; DbOrders+=('DATABASE_ROOT_PASSWORD')

declare -A DirMenu;                                              declare -a DirOrders
DirMenu['APP_USER']='user ' ; DirOrders+=('APP_USER')
DirMenu['APP_USERID']='user id ' ; DirOrders+=('APP_USERID')
DirMenu['APP_USERGID']='user gid ' ; DirOrders+=('APP_USERGID')
DirMenu['APP_DIR']='www directory path, e.g. "/home/user/www"' ; DirOrders+=('APP_DIR')
DirMenu['SUBMODULES']='git submodules (need for x86 machines)' ; DirOrders+=('SUBMODULES')

declare -A DomainMenu;                            declare -a DomainOrders
DomainMenu['PROJECT_NAME']='Project name'       ; DomainOrders+=('PROJECT_NAME')
DomainMenu['DOMAIN']='project domain'           ; DomainOrders+=('DOMAIN')
DomainMenu['WEBMASTER_EMAIL']='webmaster email' ; DomainOrders+=('WEBMASTER_EMAIL')

declare -A GauthMenu;                                    declare -a GauthOrders
GauthMenu['GOOGLEFILE']='Google auth html token filename'        ; GauthOrders+=('GOOGLEFILE')
GauthMenu['GOOGLE_CLIENT_ID']='Google auth Id'                   ; GauthOrders+=('GOOGLE_CLIENT_ID')
GauthMenu['GOOGLE_CLIENT_SECRET']='Google auth secret'           ; GauthOrders+=('GOOGLE_CLIENT_SECRET')
GauthMenu['OAUTH_SECRET']='Oauth secret'                         ; GauthOrders+=('OAUTH_SECRET')

declare -A EnvList;                      declare -a EnvIndex
EnvList['dev']='Development'              ; EnvIndex+=('dev')
EnvList['test']='Test'                    ; EnvIndex+=('test')
EnvList['prod']='Production'              ; EnvIndex+=('prod')

declare -A Defaults=(
[APP_DIR]="/home/$APP_USER/www"
[SUBMODULES]='dockerfiles/submodules'
[DATABASE_PORT]=3306
[DATABASE_NAME]=symfony
[APP_USER]="$APP_USER"
[APP_USERID]="$APP_USERID"
[APP_USERGID]="$APP_USERGID"
[GOOGLEFILE]="$GOOGLEFILE"
)
#--------------------------------------------------------------------------------------------------------
function jumpto
{
    label="$1"
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}
#--------------------------------------------------------------------------------------------------------
function setAcl {
    setfacl -Rdm u:$APP_USER:rwx,g:$APP_USER:rwx,u:nobody:rwx,g:nogroup:rwx $1
}

function MakeSubDir {
    if [ ! -d $1 ]; then
        mkdir $1
        setAcl $1
    fi
}

function readVar {
local envvar="$1"
local result=$(grep "$envvar=" "$ENVFILE")
result="${result//"$envvar="/""}"
echo "$result"
}

function getVar {
local varname="$1"
local result=$(readVar "$varname")
if [ -z "$result" ]; then
    local result="NOT_CONFIGURED"
    for i in "${!Defaults[@]}"; do
        if [ "$i" = "$varname" ]; then result="${Defaults[$i]}"; fi
    done
fi
echo "$result"
}

function getList {
declare -n list="$1"
    # get current selected services
local listed=$(readVar "$2")
    #split by space
listed=$("$SBINDIR"SplitBySpaces "$listed")
    #remove double quotes
listed="${listed//'"'/''}"
    IFSBACKUP="$IFS"
    IFS=$'\n'
list=(${listed})
    IFS="$IFSBACKUP"
}

function setVar {
 local varname="$1"
 local value="$2"
 value="${value//"/"/"\/"}"
 local VARINFILE=$(grep "$varname=" "$ENVFILE")
 VARINFILE=("$VARINFILE")
if [ "$VARINFILE" ]; then
    sed -i "s/^${varname}=.*/${varname}=${value}/" "$ENVFILE"
else
    echo "$varname=$value" >> "$ENVFILE"
fi
}

function MakeMenu {
local -n contents="$1"
local -n menu="$2"
local -n orders="$3"
for i in "${orders[@]}"; do
    contents+=( "$i" )
    contents+=( "${menu[$i]}" )
done
}

function Menu {
# build menu
 MENU="$2"
 TITLE="$3"
 local -n contents="$1"
echo $(dialog --clear \
         --default-item "$4" \
         --ok-label "Enter" \
         --cancel-label "Exit" \
         --backtitle "$BACKTITLE" \
         --title "$TITLE" \
         --menu "$MENU" \
         $HEIGHT $WIDTH $CHOICE_HEIGHT \
         "${contents[@]}" \
         2>&1 >$TERMINAL)
}

function MakeForm {
# build form
local -n contents="$1"
local -n menu="$2"
local -n orders="$3"
local COUNT=1
#MenuEntries=()
 let local maxLabelWidth=0
 let local maxFieldWidth=0
for i in "${orders[@]}"; do
    local value="${menu[$i]}"
    let local width=$(expr length "$value")
    if (( "$width">"$maxLabelWidth" )); then maxLabelWidth="$width"; fi
    local value=$(getVar "$i")
    let width=$(expr length "$value")
    if (( "$width">"$maxFieldWidth" )); then maxFieldWidth="$width"; fi
done
 let maxLabelWidth="$maxLabelWidth"+3
 let maxFieldWidth="$maxFieldWidth"+2
 let local formWidth="$maxLabelWidth"+"$maxFieldWidth"
if (( "$maxFieldWidth" < 40 )); then let maxFieldWidth=40; fi
if (( "$formWidth" > "$SCREEN_WIDTH" )); then let maxFieldWidth="$SCREEN_WIDTH"-"$maxLabelWidth"-2; fi

#create form fields
for i in "${orders[@]}"; do
    contents+=( "${menu[$i]}" )
    contents+=( "${COUNT}" )
    contents+=( 2 )
    value=$(getVar "$i")
    setVar "$i" "$value"
    contents+=( "$value" )
    contents+=( "${COUNT}" )
    contents+=( "$maxLabelWidth" )
    contents+=( "$maxFieldWidth" )
    contents+=( 255 )  #сейчас максимальная длина вводимого текста 255. разобраться, можно ли сделать лучше, чем указание фиксированной длины
    ((COUNT++))
done
}

function Form {

 local FORM="$2"
 local TITLE="$3"
 local -n contents="$1"

echo $(dialog --clear \
        --backtitle "$BACKTITLE" \
        --title "$TITLE" \
        --form "$FORM" \
        "$HEIGHT" "$WIDTH" "$CHOICE_HEIGHT" \
        "${contents[@]}" \
        2>&1 >"$TERMINAL")
}

function saveVars {
 local section="$1"
 local dataset="$2"
 section="$section""Orders[@]"
 local COUNT=1
for i in "${!section}"
do
    local value=$(echo "$dataset" | cut -d" " -f"$COUNT")
    setVar "$i" "$value"
    ((COUNT++))
done
}

function MakeChecklist {
 declare -n content="$1"
 local storage="$YAMLDIR"
 shopt -s nullglob
    IFSBACKUP="$IFS"
    IFS=$'\n'
 local services=($(ls "$storage"))
    IFS="$IFSBACKUP"
declare -a selected
getList selected "$SERVICES_VAR_NAME"
#MenuEntries=()
for i in "${!services[@]}"; do
  local isSelected="off"
  for idx in "${!selected[@]}"; do
    if [ "${selected[$idx]}" = "${services[$i]}" ]; then local isSelected="on"; fi
  done
    local string="${services[$i]}"
    content+=( "${services[$i]}" )
    content+=( "$isSelected" )
done
}

function ServicesCheckList {
 declare -a contents
 MakeChecklist contents
 local name="$1"
 local title="$2"
SELECTED_SERVICES=$(dialog --clear \
    --stdout \
        --no-items \
        --backtitle "$BACKTITLE" \
        --title "$title" \
        --checklist "$name" \
        "$HEIGHT" "$WIDTH" "$CHOICE_HEIGHT" \
        "${contents[@]}")
SELECTED_STATUS=$?
}

function EnvMenu {
# select environment menu
 local actualEnv=$(getVar "ENV")
 if [ $actualEnv = "NOT_CONFIGURED" ]; then actualEnv="prod"; setVar "ENV" $actualEnv; fi

 declare -a radioList
 local -n menu="EnvList"
 local -n orders="EnvIndex"

 for i in "${orders[@]}"; do
    radioList+=( "$i" )
    radioList+=( "${menu[$i]}" )
    if [ $i = $actualEnv ]; then radioList+=( "on" ); else radioList+=( "off" ); fi
 done

 SELECTED_ENV=$(dialog --clear \
         --backtitle "$BACKTITLE" \
         --title "$TITLE" \
         --radiolist "$MENU" \
         $HEIGHT $WIDTH $CHOICE_HEIGHT\
         "${radioList[@]}" \
         2>&1 >$TERMINAL)
 SELECTED_STATUS=$?
}

function doForm {
 local section="$1"
 local form="$2"
 local title="$3"
    declare -a content
    MakeForm content "$section""Menu" "$section""Orders"
    CHOICE=$(Form "content" "$form" "$title")
    if [ "$CHOICE" ]; then saveVars "$section" "$CHOICE"; fi
}

function getComposeFiles {
 declare -a services
 getList services "$SERVICES_VAR_NAME"
 local servicesList=''
    for i in "${!services[@]}"; do
        servicesList+="-f $YAMLDIR/${services[$i]} "
    done
 echo "$servicesList"
}

#--------------------------------------------------------------------------------------------------------
MakeSubDir ./dockerfiles/submodules
MakeSubDir ./letsencrypt
MakeSubDir ./googleauth
MakeSubDir ./log
touch ./log/traefik.log
touch ./log/traefik-access.log
touch ./log/nginx-access.log
touch ./log/nginx-error.log
MakeSubDir ./storage
MakeSubDir ./storage/mongodb
MakeSubDir ./storage/mongocfg
MakeSubDir ./storage/database
MakeSubDir ./storage/redis
MakeSubDir ./storage/redis/data
MakeSubDir ./storage/rabbitmq
MakeSubDir ./storage/rabbitmq/data
#--------------------------------------------------------------------------------------------------------
jumpto "$1"
#--------------------------------------------------------------------------------------------------------
MenuConfig:
DEFAULT_ITEM=0

MainMenu:

setVar "YAMLDIR" "$YAMLDIR"

# Set or not path to xdebug conditionally
if [ $(getVar "ENV") = "dev" ]; then setVar "XDEBUG_INI" "$XDEBUG_INI"; else setVar "XDEBUG_INI" /dev/null; fi

declare -a content
MakeMenu content "MainMenu" "menuorders"
CHOICE=$(Menu content "Options" "Main Menu" "$DEFAULT_ITEM")

case "$CHOICE" in
  0  ) #Environment
        EnvMenu "Environments" "Select environment" "Select project environment (dev/test/prod)"
        if [ "$SELECTED_STATUS" -eq 0 ]; then setVar "ENV" "$SELECTED_ENV"; fi
        DEFAULT_ITEM=0
     jumpto MainMenu
        ;;
  1  ) #Paths
        doForm "Dir" "Path menu" "Project dirs path settings"
        DEFAULT_ITEM=1
     jumpto MainMenu
        ;;
  2  ) #Domain settings
        doForm "Domain" "Project domain menu" "Project domain settings"
        DEFAULT_ITEM=2
     jumpto MainMenu
        ;;
  3  )  #Database settings
        doForm "Db" "Databse menu" "Database connection settings"
        DEFAULT_ITEM=3
     jumpto MainMenu
        ;;
  4  ) #Google auth settings
        doForm "Gauth" "Google auth menu" "Traefik Google auth settings"
        DEFAULT_ITEM=4
     jumpto MainMenu
        ;;
  5  )  #services
        ServicesCheckList "Check desired services" "check docker-compose services to run"
        if [ "$SELECTED_STATUS" -eq 0 ]; then setVar "$SERVICES_VAR_NAME" "$SELECTED_SERVICES"; fi
        DEFAULT_ITEM=5
     jumpto MainMenu
        ;;
esac
jumpto eXit

servicesUp:
    cmd="docker-compose $(getComposeFiles)up -d"
    $($cmd)

    jumpto eXit

servicesDown:
    docker-compose $(getComposeFiles)"down"
    jumpto eXit

servicesBuild:
    docker-compose $(getComposeFiles)"build"

eXit:
