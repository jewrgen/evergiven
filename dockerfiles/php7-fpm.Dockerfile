FROM alpine:3.13
#FROM alpine:3.16

ARG MAINTAINER
ARG APP_USER
ARG APP_USERID
ARG APP_USERGID
ARG ENV

LABEL maintainer=${MAINTAINER}

#RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv
RUN apk add --no-cache gnu-libiconv
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
    apk --update add --no-cache \
        php7 \
        php7-bcmath \
        php7-dom \
        php7-ctype \
        php7-curl \
        php7-fileinfo \
        php7-fpm \
        php7-gd \
        php7-iconv \
        php7-intl \
        php7-json \
        php7-mbstring \
        php7-mcrypt \
        php7-mysqlnd \
        php7-opcache \
        php7-openssl \
        php7-pdo \
        php7-pdo_mysql \
        php7-mysqli \
        php7-pdo_pgsql \
        php7-pdo_sqlite \
        php7-phar \
        php7-posix \
        php7-simplexml \
        php7-session \
        php7-soap \
        php7-tokenizer \
        php7-xml \
        php7-xmlreader \
        php7-xmlwriter \
        php7-zip \
        php7-redis \
        php7-xsl \
        php7-amqp \
        php7-sodium \
        php7-sockets \
        php7-mongodb \
        php7-pecl-mongodb \
        yarn \
        git \
        bash \
        wget \
        acl \
        attr \
        python3 \
        g++ \
        make \
        libsass-dev && \
        if [[ "$ENV" != 'prod' ]] ; then \
            apk --update add --no-cache \
            php7-xdebug ; \
        fi && \
        rm -rf /var/cache/apk/*

#EXPOSE 9000

RUN ln -sf /usr/bin/php7 /usr/bin/php

CMD ["php-fpm7", "-F"]

# install composer
RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

# create non-root user
RUN adduser --disabled-password --gecos '' ${APP_USER} -u ${APP_USERID} -g ${APP_USERGID}

# install symfony cli from user
RUN su ${APP_USER} -c 'wget https://get.symfony.com/cli/installer -O - | bash'
RUN echo 'export PATH="$HOME/.symfony5/bin:$PATH"' >> /home/${APP_USER}/.bashrc

# configure git from user
RUN su ${APP_USER} -c 'git config --global user.email ${MAINTAINER}'
RUN su ${APP_USER} -c 'git config --global user.name ${APP_USER}'

# set bash comman prompt colors for user
RUN echo 'PS1="\[\033]0;\u@\h:\w\007\]\[\033[01;31m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\]"' >> /home/${APP_USER}/.bashrc

# cd to /var/www on user login
RUN echo "cd /var/www" >> /home/${APP_USER}/.bashrc
