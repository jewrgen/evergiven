FROM alpine:3.16.2

ARG MAINTAINER
ARG APP_USER
ARG APP_USERID
ARG APP_USERGID
ARG ENV

LABEL maintainer=${MAINTAINER}

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
    apk --update add --no-cache \
        php81 \
        php81-bcmath \
        php81-dom \
        php81-ctype \
        php81-curl \
        php81-fileinfo \
        php81-fpm \
        php81-gd \
        php81-iconv \
        php81-intl \
        php81-json \
        php81-mbstring \
#        php81-mcrypt \
        php81-pecl-mcrypt \
        php81-mysqlnd \
        php81-opcache \
        php81-openssl \
        php81-pdo \
        php81-pdo_mysql \
        php81-mysqli \
        php81-pdo_pgsql \
        php81-pdo_sqlite \
        php81-phar \
        php81-posix \
        php81-simplexml \
        php81-session \
        php81-soap \
        php81-tokenizer \
        php81-xml \
        php81-xmlreader \
        php81-xmlwriter \
        php81-zip \
        php81-redis \
        php81-xsl \
#        php81-amqp \
        php81-pecl-amqp \
        php81-sodium \
        php81-sockets \
#        php81-mongodb \
        php81-pecl-mongodb \
        yarn \
        git \
        bash \
        wget \
        acl \
        attr \
        python3 \
        g++ \
        make \
        libsass-dev && \
        if [[ "$ENV" != 'prod' ]] ; then \
            apk --update add --no-cache \
            php81-xdebug ; \
        fi && \
        rm -rf /var/cache/apk/*

#EXPOSE 9000

RUN ln -sf /usr/bin/php81 /usr/bin/php

CMD ["php-fpm81", "-F"]

# install composer
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

# create non-root user
RUN adduser --disabled-password --gecos '' ${APP_USER} -u ${APP_USERID} -g ${APP_USERGID}

# install symfony cli from user
RUN su ${APP_USER} -c 'wget https://get.symfony.com/cli/installer -O - | bash'
RUN echo 'export PATH="$HOME/.symfony5/bin:$PATH"' >> /home/${APP_USER}/.bashrc

# configure git from user
RUN su ${APP_USER} -c 'git config --global user.email ${MAINTAINER}'
RUN su ${APP_USER} -c 'git config --global user.name ${APP_USER}'

# set bash comman prompt colors for user
RUN echo 'PS1="\[\033]0;\u@\h:\w\007\]\[\033[01;31m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\]"' >> /home/${APP_USER}/.bashrc

# cd to /var/www on user login
RUN echo "cd /var/www" >> /home/${APP_USER}/.bashrc
